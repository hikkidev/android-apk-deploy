import os
import argparse
import requests
import json
import re


def get_board_id(key, token, target_board_name):
    print("Find board:" + target_board_name)
    get_boards_url = "https://api.trello.com/1/members/me/boards?key=" + \
        key + "&token=" + token + "&response_type=token"

    r = requests.get(get_boards_url)
    for boards in r.json():

        board_id = ""
        board_name = ""
        for key, value in boards.items():
            if key == "id":
                board_id = value
            elif key == "name":
                board_name = value

        if board_name == target_board_name:
            return board_id

    print("Didn't find board.")
    return False


def get_list_id(key, token, board_id, target_list_name):
    print("Find list:" + target_list_name)
    get_lists_url = "https://api.trello.com/1/boards/" + board_id + \
        "/lists?key=" + key + "&token=" + token + "&response_type=token"

    r = requests.get(get_lists_url)

    for lists in r.json():

        list_id = ""
        list_name = ""

        for key, value in lists.items():
            if key == "id":
                list_id = value
            elif key == "name":
                list_name = value
        if list_name == target_list_name:
            return list_id

    print("Didn't find list.")
    return False


def get_gitlab_label_id(key, token, board_id):
    querystring = {
        "name": "GitLab",
        "color": "orange",
        "keepFromSource": "all",
        "key": key,
        "token": token
    }

    url = 'https://api.trello.com/1/boards/%s/labels' % board_id

    response = requests.request("POST", url, params=querystring)
    if response.status_code == 200:
        return response.json()['id']
    return False


def attach_file_to_trello_card(key, token, card_id, file_path):
    """
    Upload a local file to a Trello card as an attachment.
    :param key: Trello API app key
    :param token: Trello user access token - must have write access
    :param card_id: The relevant card id
    :param file_path: path to the file to upload
    Returns a request Response object. It's up to you to check the
        status code.
    """
    print("Upload a local file to a Trello card as an attachment...")
    params = {'key': key, 'token': token}
    files = {'file': open(file_path, 'rb')}
    url = 'https://api.trello.com/1/cards/%s/attachments' % card_id
    return requests.post(url, params=params, files=files)


def create_trello_card(key, token, list_id, label_id, name, desc):
    print("Creating new card in Trello")
    querystring = {
        "name": name,
        "desc": desc,
        "idList": list_id,
        "idLabels": label_id,
        "keepFromSource": "all",
        "key": key,
        "token": token
    }

    response = requests.request(
        'POST', 'https://api.trello.com/1/cards', params=querystring)
    if response.status_code == 200:
        return response.json()['id']
    return False


def get_app(release_dir, project_num):
    '''Extract app data

    Args:
        release_dir (str): Path to release directory.
    Returns:
        (str, str, str): Card name, File name and Path to release apk file.
    '''
    print("Extract app data")
    output_path = os.path.join(release_dir, 'output.json')

    with(open(output_path)) as app_output:
        json_data = json.load(app_output)

    apk_details_key = ''
    if 'apkInfo' in json_data[0]:
        apk_details_key = 'apkInfo'
    elif 'apkData' in json_data[0]:
        apk_details_key = 'apkData'
    else:
        print("Failed: parsing json in output file")
        return None, None, None
    app_file_name = json_data[0][apk_details_key]['outputFile']
    app_file_path = os.path.join(release_dir, app_file_name)
    card_name = project_num + " " + " New " + json_data[0][apk_details_key]['baseName'] + " build v" + json_data[0][apk_details_key]['versionName'] + " available"
    return card_name, app_file_name, app_file_path


def get_changes(change_log_path):
    '''Extract latest changes from changelog file.
    Changes are separated by ---
    Args:
        change_log_path (str): Path to changelog file.
    Returns:
        str: Latest changes.
    '''
    print("Extract latest changes from changelog file")
    with(open(change_log_path)) as change_log_file:
        change_log = change_log_file.read()

    latest_version_changes = change_log.split('---')[0][:-1]

    return latest_version_changes

def try_to_get_exists_card_id(key, token, list_id, target_card_name):
    print("Search card " + target_card_name)
    url = 'https://api.trello.com/1/lists/%s/cards?fields=id,name&key=%s&token=%s' %  (list_id, key, token)
    response = requests.request('GET', url)
    if response.status_code == 200:
        for cards in response.json():
            if cards['name'] == target_card_name:
                return cards['id']
    return False

if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--release.dir', dest='release_dir',
                        help='Path to release folder', required=True)
    parser.add_argument('--changelog.file', dest='changelog_file',
                        help='Path to changelog file', required=True)
    parser.add_argument('--trello.key', dest='trello_key',
                        help='Trello API app key', required=True)
    parser.add_argument('--trello.token',  dest='trello_token',
                        help='Trello API user access token', required=True)
    parser.add_argument('--trello.boardname', dest='board_name',
                        help='Trello API app key', required=True)
    parser.add_argument('--trello.listname', dest='list_name',
                        help='Trello API app key', required=True)
    parser.add_argument('--trello.projectid',  dest='project_num',
                        help='Number of parent card: Project#NUMBER', required=True)

    options = parser.parse_args()

    # Extract app version and file
    card_name, app_file_name, app_file_path = get_app(
        options.release_dir, options.project_num)
    if app_file_path == None:
        print("APK file not found")
        exit(2)

    # Find board id by name
    board_id = get_board_id(options.trello_key,
                            options.trello_token, options.board_name)
    if board_id == False:
        exit(3)

    # Find list id by name
    list_id = get_list_id(options.trello_key,
                          options.trello_token, board_id, options.list_name)
    if list_id == False:
        exit(4)

    # Create or get exists label ID
    label_id = get_gitlab_label_id(
        options.trello_key, options.trello_token, board_id)
    if label_id == False:
        exit(5)

    # Extract latest changes
    latest_changes = get_changes(options.changelog_file)
    if latest_changes == None:
        exit(6)

    alreadyExistsId = try_to_get_exists_card_id(options.trello_key, options.trello_token, list_id, card_name)
    if alreadyExistsId != False:
        print("Upload build to exsisting card")
        res = attach_file_to_trello_card(options.trello_key, options.trello_token, alreadyExistsId, app_file_path)
        print(res)
        exit(0)

    # Create card
    card_id = create_trello_card(
        options.trello_key, options.trello_token, list_id, label_id, card_name, latest_changes)
    if card_id == False:
        exit(8)

    res = attach_file_to_trello_card(options.trello_key, options.trello_token, card_id, app_file_path)
    print(res)
